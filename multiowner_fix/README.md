# Allow working diferent sites with different owners

For the normal work of the sites, it is necessary that all new files are created with the rights 0775 (instead of 0755). The main modx's core can be configured using the settings (new_file_permissions  and new_folder_permissions  ), but some components require code editing. When updating, be careful!

To configure, you must:

* Add system setting: `new_file_permissions` and `new_folder_permissions` with value 0775.
* Change premision of folders/files to 775 (`chmod -R 775 <path>`).
* Fix xFPC: 
	* [plugin xFPC](plugins/xFPC.php)
* Fix modxminify: 
	* [file AssetWriter](files/core/components/modxminify/assetic/src/Assetic/AssetWriter.php)
* Fix phpthumbsup:
	* [file AssetWriter](files/core/components/phpthumbsup/model/phpthumbsup/phpthumbsup.class.php)


